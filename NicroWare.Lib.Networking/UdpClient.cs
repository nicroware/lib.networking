﻿using System;
using System.Net.Sockets;
using System.IO;
using System.Collections.Generic;
using System.IO.Ports;
using NicroWare.Lib.Networking.Logger;

namespace NicroWare.Lib.Networking
{
    public delegate void UpdateEventHandler(object sender, EventArgs e);
    internal class DummyLogger : ILogger
    {
        #region ILogger implementation

        public void WriteLine(string text, string category, int level)
        {
            
        }

        public void WriteLine(string text, string category, string sender, int level)
        {
            
        }

        public void WriteException(string text, Exception exception, int level)
        {
            
        }

        public void WriteException(string text, Exception exception, string sender, int level)
        {
            
        }

        #endregion



    }

    internal class UDPStreamReciver
    {
        public Stream _baseStream;
        public Stream baseStream
        { 
            get
            { 
                if (port != null)
                    return port.BaseStream;
                else
                    return _baseStream; 
            }
        }
        public SerialPort port;
        static Dictionary<Stream, UDPStreamReciver> allBindings = new Dictionary<Stream, UDPStreamReciver>();
        static Dictionary<SerialPort, UDPStreamReciver> allPortBindings = new Dictionary<SerialPort, UDPStreamReciver>();
        public byte[] buffer;
        public byte[] prevBuffer;
        int aviableBytes = 0;
        int currentByte = 0;
        int prevAviableBytes = 0;
        int prevByte = 0;
        const int bufferSize = 1024;
        public event UpdateEventHandler OnPackageRecieved;
        ILogger logger;

        public UDPStreamReciver(Stream baseStream, ILogger logger)
        {
            this._baseStream = baseStream;
            buffer = new byte[bufferSize];
            if (logger == null)
                logger = new DummyLogger();
            else
                this.logger = logger;
        }

        public UDPStreamReciver(SerialPort baseStream, ILogger logger)
        {
            this.port = baseStream;
            buffer = new byte[bufferSize];
            if (logger == null)
                logger = new DummyLogger();
            else
                this.logger = logger;
        }

        public static UDPStreamReciver GetReciver(Stream stream, ILogger logger)
        {
            if (!allBindings.ContainsKey(stream))
                allBindings.Add(stream, new UDPStreamReciver(stream, logger));
            return allBindings[stream];
        }

        public static UDPStreamReciver GetReciver(SerialPort stream, ILogger logger)
        {
            if (!allPortBindings.ContainsKey(stream))
                allPortBindings.Add(stream, new UDPStreamReciver(stream, logger));
            return allPortBindings[stream];
        }

        public void Read(byte[] buffer, int offset, int count, bool header, out bool shortHeader)
        {
            logger.WriteLine("Reading. Aviable: " + aviableBytes + " Current: " + currentByte, "INFO", 1);
            int writeOffset = 0;
            shortHeader = false;
            if (currentByte + count >= aviableBytes)
            {
                if (header && currentByte + 2 <= aviableBytes)
                {
                    shortHeader = AdjustCurrentByte();
                    currentByte += 2;
                }
                writeOffset = aviableBytes - currentByte;
                Array.Copy(this.buffer, currentByte, buffer, 0, writeOffset);
                currentByte = aviableBytes;
            }
            if (currentByte == aviableBytes)
            {
                prevBuffer = buffer;
                prevAviableBytes = currentByte;
                prevByte = currentByte;
                this.buffer = new byte[bufferSize];
                aviableBytes = 0;//baseStream.Read(buffer, 0, buffer.Length);
                currentByte = 0;
                logger.WriteLine("Startet reading from stream", "INFO", 1);
                while (currentByte == aviableBytes || aviableBytes - currentByte  < count - writeOffset + (header ? 2 : 0))
                {
                    //TODO: Can overflow if magic number is not found
                    aviableBytes += baseStream.Read(this.buffer, aviableBytes, bufferSize - aviableBytes);
                    logger.WriteLine("Adding aviable bytes. Current: " + aviableBytes, "INFO", 1);
                    if (writeOffset == 0 && header)
                        shortHeader = AdjustCurrentByte();
                }
				//Console.WriteLine ("Read: " + aviableBytes);
//				List<byte> allBytes = new List<byte> ();
//				string path = "/home/espen/mono/debug.bin";
//				if (File.Exists(path))
//					allBytes.AddRange (File.ReadAllBytes (path));
//				for (int i = 0; i < aviableBytes; i++)
//					allBytes.Add (this.buffer [i]);
//				File.WriteAllBytes (path, allBytes.ToArray ());
            }
            if (header)
            {
                shortHeader = AdjustCurrentByte();
                currentByte += 2;
                logger.WriteLine("Aviable bytes: " + aviableBytes + " Current bytes: " + currentByte, "INFO", 1);
            }
            Array.Copy(this.buffer, currentByte, buffer, writeOffset, count - writeOffset);
            currentByte += count - writeOffset; 
        }

        public bool AdjustCurrentByte()
        {
            for (; currentByte < aviableBytes - 2; currentByte++)
            {
                /*if (this.buffer[currentByte] == 0
                    && this.buffer[currentByte + 1] == (byte)'I'
                    && this.buffer[currentByte + 2] == (byte)'O'
                    && this.buffer[currentByte + 3] == (byte)'N')
                {
                    //currentByte += 4;
                    return false;
                }*/
                if (this.buffer[currentByte] == 0x49
                    && this.buffer[currentByte + 1] == 0x4F)
                {
                    return false;
                }
                else if (this.buffer[currentByte] == 0x69
                    && this.buffer[currentByte + 1] == 0x4F)
                {
                    return true;
                }
            }
            return false;
            /*
            if (currentByte == aviableBytes - 4)
                currentByte = aviableBytes;*/
        }
    }

    public class UDPStreamClient
    {
        UDPStreamReciver reciver;
        ushort localPort;
        ushort destinationPort;

        public UDPStreamClient(Stream baseStream, ushort port, ILogger logger)
        {
            this.localPort = port;
            //UdpClient client = new UdpClient(66);
            reciver = UDPStreamReciver.GetReciver(baseStream, logger);
        }

        public UDPStreamClient(SerialPort basePort, ushort port, ILogger logger)
        {
            this.localPort = port;
            //UdpClient client = new UdpClient(66);
            reciver = UDPStreamReciver.GetReciver(basePort, logger);
        }

        public UDPStreamClient(Stream baseStream, ushort port)
            :this(baseStream, port, null)
        {
        }



        public void Connect(ushort destPort)
        {
            this.destinationPort = destPort;
        }

        public int Send(byte[] dtgr, int length)
        {
            bool shortHeader = true;
            UDPHeader header = new UDPHeader();
            header.SourcePort = localPort;
            header.DestinationPort = destinationPort;
            header.Length = (ushort)dtgr.Length;
            List<byte> list = new List<byte>();
            if (!shortHeader)
                list.AddRange(new byte[] { 0x49, 0x4F });
            else
                list.AddRange(new byte[] { 0x69, 0x4F });
            
            list.AddRange(header.GetBytes(dtgr, shortHeader));


            
            //reciver.baseStream.Write(new byte[]{ 0, (byte)'I', (byte)'O', (byte)'N' }, 0, 4);
            //reciver.baseStream.Write(bytes, 0, bytes.Length);
            reciver.baseStream.Write(list.ToArray(), 0, list.Count);

            reciver.baseStream.Flush();
            if (reciver.baseStream.CanSeek)
                reciver.baseStream.Seek(-(list.Count - 2), SeekOrigin.Current);
            return 0;
        }

        public byte[] Recieve()
        {
            byte[] returnData = null;
            bool recived = false;
            while (!recived)
            {
                byte[] udpData;
                //UDPHeader header = new UDPHeader().ReadHeader(reciver.baseStream, out udpData);
                UDPHeader header = new UDPHeader().ReadHeader(reciver, out udpData);
                if (header == null)
                {
                    Console.WriteLine("Recieved empty header");
                    continue;
                }
                //Console.WriteLine("Recived header: " + header.DestinationPort);
                if (header.DestinationPort == localPort)
                {
                    returnData = udpData;
                    recived = true;
                }
            }
            return returnData;
        }
    }

    public class UDPHeader
    {
        public ushort SourcePort { get; set; }
        public ushort DestinationPort { get; set; }
        public ushort Length { get; set; }
        public ushort Checksum { get; set; }

        public byte[] GetBytes(byte[] data, bool shortFormat)
        {
            Length = (ushort)data.Length;
            Checksum = CalculateChecksum(data, shortFormat);
            List<byte> bytes = new List<byte>();
            if (!shortFormat)
            {
                bytes.AddRange(BitConverter.GetBytes(SourcePort));
                bytes.AddRange(BitConverter.GetBytes(DestinationPort));
                bytes.AddRange(BitConverter.GetBytes(Length));
                bytes.AddRange(BitConverter.GetBytes(Checksum));
                bytes.AddRange(data);
            }
            else
            {
                bytes.Add((byte)DestinationPort);
                bytes.Add((byte)Length);
                bytes.AddRange(BitConverter.GetBytes(Checksum));
                bytes.AddRange(data);
            }
            return bytes.ToArray();
        }

        public ushort CalculateChecksum(byte[] data, bool shortFormat)
        {
            int checkSumCalc = 0;
            //int 
            if (!shortFormat)
            {
                checkSumCalc = SourcePort + DestinationPort + Length + data.GetSum();
                while (checkSumCalc >= ushort.MaxValue)
                {
                    checkSumCalc = (checkSumCalc >> 8) + (checkSumCalc & byte.MaxValue);
                }
            }
            else
            {
                checkSumCalc = (byte)DestinationPort + (byte)Length + data.GetSum();
                while (checkSumCalc >= ushort.MaxValue)
                {
                    checkSumCalc = (checkSumCalc >> 8) + (checkSumCalc & byte.MaxValue);
                }
            }
            return (ushort)checkSumCalc;
        }

        public UDPHeader ReadHeader(Stream stream, out byte[] data)
        {
            //Console.WriteLine("Read header");
            byte[] header = new byte[8];
            //stream.Read(header, 0, header.Length);
			SafeRead(stream, header, 0, header.Length);
            SourcePort = BitConverter.ToUInt16(header, 0);
            DestinationPort = BitConverter.ToUInt16(header, 2);
            Length = BitConverter.ToUInt16(header, 4);
            Checksum = BitConverter.ToUInt16(header, 6);
            data = new byte[Length];
			SafeRead(stream, data, 0, data.Length);
            //Array.Copy(, 8, data, 0, Length);
            if (Checksum != CalculateChecksum(data, false))
            {
                return null;
            }
            return this;
        }

        internal UDPHeader ReadHeader(UDPStreamReciver reciver, out byte[] data)
        {
            //Console.WriteLine("Read header");
            byte[] header = new byte[4];
            bool shortFormat;
            reciver.Read(header, 0, header.Length, true, out shortFormat);
            data = new byte[0];
            if (!shortFormat)
            {
                SourcePort = BitConverter.ToUInt16(header, 0);
                DestinationPort = BitConverter.ToUInt16(header, 0);
                if (Length > 256)
                    return null;
                Length = BitConverter.ToUInt16(header, 4);
                Checksum = BitConverter.ToUInt16(header, 6);
            }
            else 
            {
                //SourcePort = BitConverter.ToUInt16(header, 0);
                DestinationPort = header[0];//BitConverter.ToUInt16(header, 0);
                Length = header[1];//BitConverter.ToUInt16(header, 4);
                Checksum = BitConverter.ToUInt16(header, 2);
            }

            //Checksum = BitConverter.ToUInt16(header, 6);
            data = new byte[Length];
            bool temp;
            reciver.Read(data, 0, data.Length, false, out temp);
            if (Checksum != CalculateChecksum(data, shortFormat))
                return null;
            return this;
        }

		private void SafeRead(Stream stream, byte[] data, int offset, int length)
		{
			for (int i = 0; i < length;) 
			{
				i += stream.Read(data, i, length - i);
			}
		}

        public UDPHeader ReadHeader(byte[] bytes, out byte[] data)
        {
            SourcePort = BitConverter.ToUInt16(bytes, 0);
            DestinationPort = BitConverter.ToUInt16(bytes, 2);
            Length = BitConverter.ToUInt16(bytes, 4);
            Checksum = BitConverter.ToUInt16(bytes, 6);
            data = new byte[Length];
            Array.Copy(bytes, 8, data, 0, Length);
            if (Checksum != CalculateChecksum(data, false))
            {
                return null;
            }
            return this;
        }
    }
        
    public struct DataWrapper
    {
        public ushort SensorID { get; set; }
        public int Value { get; set; }

        public byte[] GetBytes(bool shortFormat)
        {
            
            byte[] bytes;
            if (!shortFormat)
            {
                bytes = new byte[6];
                Array.Copy(BitConverter.GetBytes(SensorID), 0, bytes, 0, 2);
                Array.Copy(BitConverter.GetBytes(Value), 0, bytes, 2, 4);
            }
            else
            {
                bytes = new byte[3];
                bytes[0] = (byte)SensorID;
                Array.Copy(BitConverter.GetBytes(Value), 0, bytes, 1, 2);
            }
            return bytes;
        }

        public static DataWrapper ReadData(byte[] bytes, int offset, bool shortFormat)
        {
            DataWrapper wrapper = new DataWrapper();
            if (!shortFormat)
            {
                wrapper.SensorID = BitConverter.ToUInt16(bytes, offset);
                wrapper.Value = BitConverter.ToInt32(bytes, offset + 2);
            }
            else
            {
                wrapper.SensorID = bytes[offset];
                wrapper.Value = BitConverter.ToInt16(bytes, offset + 1);
            }
            return wrapper;
        }
    }

    public static class Extension
    {
        public static int GetSum(this byte[] bytes)
        {
            int sum = 0;
            for(int i = 0; i < bytes.Length / 2; i+= 2)
            {
                sum += BitConverter.ToUInt16(bytes, i);
            }
            return sum;
        }
    }
}

