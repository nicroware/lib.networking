﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NicroWare.Lib.Networking.Logger
{
    public interface ILogger
    {
        void WriteLine(string text, string category, int level);

        void WriteLine(string text, string category, string sender, int level);

        void WriteException(string text, Exception exception, int level);

        void WriteException(string text, Exception exception, string sender, int level);
    }

    public delegate void LogEvent(object sender, LogWriteEventArgs e);

    public class LogWriteEventArgs : EventArgs
    {
        public LogEntry Entry { get; set; }
    }

    public interface ILogManager
    {
        void WriteLine(string text, string category, string sender, int level);
        void WriteException(string text, Exception exception, string sender, int level);
        void WriteCustom(LogEntry entry);
    }

    public class LogManager : ILogManager
    {
        public int nextId = 0;
        public List<LogEntry> AllEnteries = new List<LogEntry>();
        const string EXCEPTION = "EXCEPTION";

        public event LogEvent OnVerbose;
        public event LogEvent OnInfo;
        public event LogEvent OnWarning;
        public event LogEvent OnError;
        public event LogEvent OnCritical;

        public void WriteLine(string text, string category, string name, int level)
        {
            Add(new LogEntry() { Value = text, Category = category, Level = level, Sender = name, Time = DateTime.Now });
        }

        public void WriteException(string text, Exception exception, string sender, int level)
        {
            Add(new ExceptionEntry() { Value = text, Exception = exception, Category = EXCEPTION, Level = level, Sender = sender, Time = DateTime.Now });
        }

        public void WriteCustom(LogEntry entry)
        {
            entry.Time = DateTime.Now;
            Add(entry);
        }

        private void Add(LogEntry entry)
        {
            entry.ID = nextId++;
            AllEnteries.Add(entry);
            ActivateEvent(entry);
        }

        private void ActivateEvent(LogEntry entry)
        { 
            LogWriteEventArgs args = new LogWriteEventArgs() { Entry = entry};
            if (entry.Level >= 0 && OnVerbose != null)
                OnVerbose(this, args);
            if (entry.Level >= 2 && OnInfo != null)
                OnInfo(this, args);
            if (entry.Level >= 3 && OnWarning != null)
                OnWarning(this, args);
            if (entry.Level >= 4 && OnError != null)
                OnError(this, args);
            if (entry.Level >= 5 && OnCritical != null)
                OnCritical(this, args);
        }

        public LogInterface CreateLogger(string name)
        {
            return new LogInterface(name, this);
        }
    }

    public class LogInterface : ILogger
    {
        protected ILogManager Parrent { get; private set; }
        protected string Name { get; private set; }

        public LogInterface(string name, ILogManager parrent)
        {
            this.Name = name;
            this.Parrent = parrent;
        }

        public void WriteLine(string text, string category, int level)
        {
            WriteLine(text, category, Name, level);
        }

        public void WriteLine(string text, string category, string sender, int level)
        {
            Parrent.WriteLine(text, category, sender, level);
        }

        public void WriteException(string text, Exception exception, int level)
        {
            WriteException(text, exception, Name, level);
        }

        public void WriteException(string text, Exception exception, string sender, int level)
        {
            Parrent.WriteException(text, exception, sender, level);
        }

        public void WriteCustom(LogEntry entry)
        {
            if (entry.Sender == null)
                entry.Sender = Name;
            
            Parrent.WriteCustom(entry);
        }
    }

    /*
     * 0 = Verbose Info
     * 1 = Info
     * 2 = Important Info
     * 3 = Warning
     * 4 = Error
     * 5 = Critical
     */

    public enum LogLevel : int
    { 
        VerboseInfo = 0,
        Info = 1,
        ImportantInfo = 2,
        Warning = 3,
        Error = 4,
        Critical = 5
    }

    public class LogEntry
    {
        public int ID { get; set; }
        public string Category { get; set; }
        public string Sender { get; set; }
        public int Level { get; set; }
        public DateTime Time { get; set; }
        public virtual string Value { get; set; }
    }

    public class ExceptionEntry : LogEntry
    {
        public Exception Exception { get; set; }
    }
}
