﻿using System;
using System.IO;
using System.IO.Ports;
using System.Collections.Generic;
using System.Collections;

namespace NicroWare.Lib.Networking.Manager
{
    public class NetworkManager
    {
        static Dictionary<Stream, NetworkClient> streamClients = new Dictionary<Stream, NetworkClient>();
        static Dictionary<SerialPort, NetworkClient> portClients = new Dictionary<SerialPort, NetworkClient>();

        public NetworkManager()
        {
            
        }

        public static NetworkClient BindClient(Stream stream)
        {
            if (!streamClients.ContainsKey(stream))
                streamClients.Add(stream, new NetworkClient(stream));
            return streamClients[stream];
        }

        public static NetworkClient BindClient(SerialPort port)
        {
            if (!portClients.ContainsKey(port))
                portClients.Add(port, new NetworkClient(port));
            return portClients[port];   
        }

        public static void BindClient(Stream Stream, IParseClient client)
        {
            NetworkClient netClient = BindClient(Stream);
            client.Initialize(netClient);
        }

        public static void BindClient(SerialPort port, IParseClient client)
        {
            NetworkClient netClient = BindClient(port);
            client.Initialize(netClient);
        }
    }

    public delegate NetworkPack Parser(NetworkReader reader);

    public class LengthComparer : IComparer<byte[]>
    {
        public bool Reverse { get; set; }
        public LengthComparer(bool reverse)
        {
            Reverse = reverse;
        }

        int IComparer<byte[]>.Compare(byte[] x, byte[] y)
        {
            int number = x.Length - y.Length;
            return Reverse ? number * -1 : number;
        }
    }

    public class NetworkClient
    {
        Dictionary<byte[], Parser> lookup = new Dictionary<byte[], Parser>();
        public SerialPort Port { get; private set; }
        private Stream stream { get; set; }
        public Stream Stream { get { return Port == null ? stream : Port.BaseStream; }}
        List<NetworkPack> allPacks = new List<NetworkPack>();
        public int LongestKey  { get; private set; }
        public NetworkReader Reader { get; private set; }

        private NetworkClient()
        {
            lookup = new Dictionary<byte[], Parser>(/*new LengthComparer(true)*/);
            //TODO: Implement a form of SortedDictionary
            LongestKey = 0;
            Reader = new NetworkReader(this);
        }

        public NetworkClient(Stream stream)
            : this()
        {
            this.stream = stream;
        }

        public NetworkClient(SerialPort port)
            : this()
        {
            this.Port = port;
        }

        public void AddParser(byte[] sequence, Parser parser)
        {
            lookup.Add(sequence, parser);
            if (sequence.Length > LongestKey)
                LongestKey = sequence.Length;
        }

        public NetworkPack FindPack<T>(int id)
        {
            foreach (NetworkPack pack in allPacks)
            {
                if (pack.Header.GetType() == typeof(T) && pack.Header.Id == id)
                    return pack;
            }
            return null;
        }

        public NetworkPack ReadMessage<T>(int id)
        {
            NetworkPack pack = FindPack<T>(id);
            if (pack != null)
                allPacks.Remove(pack);
            while (pack == null)
            {
                byte[] key = Reader.AddjustToMessage();
                NetworkPack np = lookup[key](Reader);
                if (np != null)
                {
                    if (np.Header.GetType() == typeof(T) && np.Header.Id == id)
                        pack = np;
                    else
                        allPacks.Add(np);
                }
            }
            return pack;
        }

        public byte[] FindHeaderStart(Func<int, byte[]> peek)
        {
            if (lookup.Keys.Count == 0)
                throw new IndexOutOfRangeException();
            byte[] bytes = peek(LongestKey);
            //Console.WriteLine("Bytes: {0} {1}", bytes[0], bytes[1]);
            foreach (KeyValuePair<byte[], Parser> pair in lookup)
            {
                bool match = true;
                for (int i = 0; i < pair.Key.Length; i++)
                {
                    if (pair.Key[i] != bytes[i])
                    {
                        match = false;
                        break;
                    }
                }
                if (match)
                {
                    return pair.Key;
                }
            }
            return new byte[0];
        }
    }

    public class NetworkReader
    {
        NetworkClient client;
        byte[,] buffer = new byte[2, 1024];
        int currentIndex { get; set; }
        int totalIndex { get; set; }
        int available  { get { return totalIndex - currentIndex; }}

        public NetworkReader(NetworkClient client)
        {
            this.client = client;
            currentIndex = 0;
            totalIndex = 0;
        }

        public void Read(byte[] buffer)
        {
            Read(buffer, 0, buffer.Length);
        }

        public void Read(byte[] buffer, int offset)
        {
            Read(buffer, offset, buffer.Length - offset);
        }

        public void Read(byte[] buffer, int offset, int length)
        {
            byte[] bytes = PeekBytes(length);
            Array.Copy(bytes, 0, buffer, offset, length);
            currentIndex += bytes.Length;
        }

        public void Write(byte[] buffer)
        {
            Write(buffer, 0, buffer.Length);
        }

        public void Write(byte[] buffer, int offset, int length)
        {
            client.Stream.Write(buffer, offset, length);
        }

        private BufferIndex ConvertToBufferIndex(int i)
        {
            return new BufferIndex(i % 1024, i / 1024 % 2);
        }

        private void FillBuffer()
        {
            byte[] tempBuffer = new byte[1024];
            int read = client.Stream.Read(tempBuffer, 0, tempBuffer.Length);
            //string s = "";
            for (int i = 0; i < read; i++)
            {
                //s += tempBuffer[i] +  " ";
                BufferIndex indexes = ConvertToBufferIndex(i + totalIndex);
                buffer[indexes.BufIndex, indexes.RealIndex] = tempBuffer[i];
            }
            //Console.WriteLine(s);
            totalIndex += read;
        }

        private void FillBuffer(int size)
        {
            while (available < size)
                FillBuffer();
        }

        private byte[] PeekBytes(int length)
        {
            FillBuffer(length);

            byte[] tempBuffer = new byte[length];
            for (int i = 0; i < length; i++)
            {
                BufferIndex bufferIndex = ConvertToBufferIndex(i + currentIndex);
                tempBuffer[i] = buffer[bufferIndex.BufIndex, bufferIndex.RealIndex];
            }
            return tempBuffer;
        }

        private class BufferIndex
        {
            public int RealIndex {get; private set; }
            public int BufIndex {get; private set; }

            public BufferIndex(int RealIndex, int BufIndex)
            {
                this.RealIndex = RealIndex;
                this.BufIndex = BufIndex;
            }
        }

        public byte[] AddjustToMessage()
        {
            FillBuffer(client.LongestKey);
            byte[] key;
            while ((key = client.FindHeaderStart(PeekBytes)).Length == 0)
            {
                currentIndex += 1;
                FillBuffer(client.LongestKey);
            }
            currentIndex += key.Length;
            return key;
        }
    }

    public interface IParseClient
    {
        void Initialize(NetworkClient client);
    }

    public class UdpNetworkClient : IParseClient
    {
        NetworkClient baseClient;
        public ushort UdpPort { get; private set; }
        public ushort DestPort { get; private set; }

        private UdpNetworkClient(ushort port) 
        {  
            this.UdpPort = port;
        }

        public UdpNetworkClient(SerialPort port, ushort udpPort)
            :this(udpPort)
        {
            NetworkManager.BindClient(port, this);
        }

        public UdpNetworkClient(Stream stream, ushort udpPort)
            :this(udpPort)
        {
            NetworkManager.BindClient(stream, this);
        }

        public void Connect(ushort destinationPort)
        {
            DestPort = destinationPort;
        }

        public void Send(byte[] bytes)
        {
            Send(bytes, false);
        }

        public void Send(byte[] data, bool shortFormat)
        {
            UdpHeader header;
            if (shortFormat)
                header = new UdpHeader(0, DestPort, (ushort)data.Length);
            else
                header = new UdpHeader(UdpPort, DestPort, (ushort)data.Length);
            byte[] bytes = header.GetBytes(data, shortFormat);
            baseClient.Reader.Write(bytes);
        }

        public byte[] Receive()
        {
            return baseClient.ReadMessage<UdpHeader>(UdpPort).Data;
        }

        void IParseClient.Initialize(NetworkClient client)
        {
            baseClient = client;
            client.AddParser(new byte[] { 0x69, 0x4f }, ParseShortUdpHeader);
            client.AddParser(new byte[] { 0x49, 0x4f }, ParseUdpHeader);
        }

        NetworkPack ParseUdpHeader(NetworkReader reader)
        {
            byte[] buffer = new byte[8];
            byte[] data = new byte[0];

            reader.Read(buffer);
            ushort sourcePort = BitConverter.ToUInt16(buffer, 0);
            ushort destinationPort = BitConverter.ToUInt16(buffer, 2);
            ushort length = BitConverter.ToUInt16(buffer, 4);
            ushort checksum = BitConverter.ToUInt16(buffer, 6);
            if (length > 255)
                return null;
            data = new byte[length];
            reader.Read(data);
            UdpHeader header = new UdpHeader(sourcePort, destinationPort, length, checksum);
            if (header.Checksum != header.CalculateCheckSum(data))
                return null;
            return new NetworkPack(header, data);

        }

        NetworkPack ParseShortUdpHeader(NetworkReader reader)
        {
            byte[] buffer = new byte[4];
            byte[] data = new byte[0];

            reader.Read(buffer);
            //Console.WriteLine("Bytes: {0} {1} {2} {3}", buffer[0], buffer[1], buffer[2], buffer[3]);
            byte destinationPort = buffer[0];
            byte length = buffer[1];
            ushort checksum = BitConverter.ToUInt16(buffer, 2);
            if (length > 24)
                return null;
            data = new byte[length];
            reader.Read(data);
            UdpHeader header = new UdpHeader(0, destinationPort, length, checksum);
            if (header.Checksum != header.CalculateCheckSum(data))
                return null;
            return new NetworkPack(header, data);
        }
    }
        
    public class NetworkPack
    {
        public NetworkPack(NetworkHeader header, byte[] data)
        {
            this.Header = header;
            this.Data = data;
        }
        public virtual NetworkHeader Header { get; private set; }
        public virtual byte[] Data { get; protected set; }
    }

    public class NetworkHeader
    {
        public int Id { get; protected set; }
    }

    public class UdpHeader : NetworkHeader
    {
        public ushort SourcePort { get; set; }
        public ushort DestinationPort { get; set; }
        public ushort Length { get; set; }
        public ushort Checksum { get; set; }

        public UdpHeader(ushort sourcePort, ushort destinationPort, ushort length)
        {
            this.SourcePort = sourcePort;
            this.DestinationPort = destinationPort;
            this.Length = length;
            this.Id = DestinationPort;
        }

        public UdpHeader(ushort sourcePort, ushort destinationPort, ushort length, ushort checksum)
        {
            this.SourcePort = sourcePort;
            this.DestinationPort = destinationPort;
            this.Length = length;
            this.Checksum = checksum;
            this.Id = DestinationPort;
        }

        public byte[] GetBytes(byte[] data, bool shortFormat)
        {
            Length = (ushort)data.Length;
            Checksum = CalculateCheckSum(data);
            List<byte> bytes = new List<byte>();
            if (!shortFormat)
            {
                //TODO: implement magic bytes better
                bytes.AddRange(new byte[] { 0x49, 0x4f });
                bytes.AddRange(BitConverter.GetBytes(SourcePort));
                bytes.AddRange(BitConverter.GetBytes(DestinationPort));
                bytes.AddRange(BitConverter.GetBytes(Length));
            }
            else
            {
                bytes.AddRange(new byte[] { 0x69, 0x4f });
                bytes.Add((byte)DestinationPort);
                bytes.Add((byte)Length);
            }
            bytes.AddRange(BitConverter.GetBytes(Checksum));
            bytes.AddRange(data);
            return bytes.ToArray();
        }

        public void AddCheckSum(byte[] data)
        {
            Checksum = CalculateCheckSum(data);
        }

        public ushort CalculateCheckSum(byte[] data)
        {
            int checkSumCalc = 0;
            checkSumCalc = SourcePort + DestinationPort + Length + data.GetSum();
            while (checkSumCalc >= ushort.MaxValue)
            {
                checkSumCalc = (checkSumCalc >> 8) + (checkSumCalc & byte.MaxValue);
            }
            return (ushort)checkSumCalc;
        }
    }

    public class CanNetworkClient : IParseClient
    {
        NetworkClient client;
        public CanNetworkClient(SerialPort port)
        {
            NetworkManager.BindClient(port, this);
        }

        public CanNetworkClient(Stream stream)
        {
            NetworkManager.BindClient(stream, this);
        }

        void IParseClient.Initialize(NetworkClient client)
        {
            this.client = client;
            client.AddParser(new byte[] { 0xFF, 0xFF }, ParseCanMessage);
        }

        public NetworkPack ParseCanMessage(NetworkReader reader)
        {
            byte[] data = new byte[3];
            reader.Read(data);
            /*ushort tempData = (ushort)(data[1] << 8);
            tempData |= data[2];
            data = BitConverter.GetBytes(tempData);*/
            //byte[] endByte = new byte[1];
            //reader.Read(endByte);
            return new NetworkPack(new CanHeader(), data);
        }

        public byte[] Receive()
        {
            return client.ReadMessage<CanHeader>(0).Data;
        }

        public void Send(byte[] data)
        {
            Console.WriteLine("Sendt!");
            client.Reader.Write(new CanHeader().GetBytes(data));
        }
        
    }

    public class CanHeader : NetworkHeader
    {
        public CanHeader()
        {
            Id = 0;
        }

        public byte[] GetBytes(byte[] data)
        {
            List<byte> allBytes = new List<byte>();
            allBytes.Add(0xFF);
            allBytes.Add(0xFF);
            allBytes.AddRange(data);
            //allBytes.Add(0x2A);
            return allBytes.ToArray();
        }
    }

    public struct DataWrapper
    {
        public ushort SensorID { get; set; }
        public int Value { get; set; }

        public byte[] GetBytes(bool shortFormat)
        {

            byte[] bytes;
            if (!shortFormat)
            {
                bytes = new byte[6];
                Array.Copy(BitConverter.GetBytes(SensorID), 0, bytes, 0, 2);
                Array.Copy(BitConverter.GetBytes(Value), 0, bytes, 2, 4);
            }
            else
            {
                bytes = new byte[3];
                bytes[0] = (byte)SensorID;
                Array.Copy(BitConverter.GetBytes(Value), 0, bytes, 1, 2);
            }
            return bytes;
        }

        public static DataWrapper ReadData(byte[] bytes, int offset, bool shortFormat)
        {
            DataWrapper wrapper = new DataWrapper();
            if (!shortFormat)
            {
                wrapper.SensorID = BitConverter.ToUInt16(bytes, offset);
                wrapper.Value = BitConverter.ToInt32(bytes, offset + 2);
            }
            else
            {
                wrapper.SensorID = bytes[offset];
                wrapper.Value = BitConverter.ToInt16(bytes, offset + 1);
            }
            return wrapper;
        }
    }
}